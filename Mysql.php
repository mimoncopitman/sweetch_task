<?php


class Mysql
{
    private $host;
    private $username;
    private $password;
    private $dbname;
    private $connection;

    /**
     * Mysql constructor.
     * @param $host
     * @param $username
     * @param $password
     * @param $dbname
     */
    public function __construct($host, $username, $password, $dbname)
    {
        $this->host = $host;
        $this->username = $username;
        $this->password = $password;
        $this->dbname = $dbname;

        $this->connection = new mysqli($this->host, $this->username, $this->password, $this->dbname);

        if ($this->connection->connect_error) die("Connection failed: " . $this->connection->connect_error);
    }

    /**
     * @return bool
     */
    public function closeConnection(): bool
    {
        return $this->connection->close();
    }

    /**
     * @param $query
     * @return bool
     */
    public function insert($query): bool
    {
        $response = false;
        if ($this->connection->query($query) === true) {
            $response = true;
        } else {
            echo "Error: {$this->connection->error}";
        }

        return $response;
    }

    /**
     * @param $query
     * @return array|null
     */
    public function fetch($query): ?array
    {
        $result = $this->connection->query($query);
        $data = [];

        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $data [] = $row;
            }
        } else {
            echo "No Results!";
            return null;
        }

        $this->closeConnection();

        return $data;
    }
}
