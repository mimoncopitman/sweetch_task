<?php

require_once 'data-params.php';
require_once 'Mysql.php';

class FileDBInsert
{
    const COLUMN_YEAR_INDEX = 0;
    const COLUMN_AGE_INDEX = 1;
    const COLUMN_ETHNIC_INDEX = 2;
    const COLUMN_SEX_INDEX = 3;
    const COLUMN_AREA_INDEX = 4;
    const COLUMN_COUNT_INDEX = 5;

    const COLUMNS = [
        self::COLUMN_YEAR_INDEX,
        self::COLUMN_AGE_INDEX,
        self::COLUMN_ETHNIC_INDEX,
        self::COLUMN_SEX_INDEX,
        self::COLUMN_AREA_INDEX,
        self::COLUMN_COUNT_INDEX,
    ];

    /**
     * Logs Messages into a log file
     * @param $message
     */
    public static function logMessage($message){
        error_log($message . PHP_EOL, 3, dirname(__FILE__) . '/app.log');
    }

    /**
     * @param $filename
     * @return false|resource
     */
    public static function getFileWithoutHeaders($filename){
        $file = fopen($filename, 'r');
        fgets($file);
        return $file;
    }

    /**
     * @param $string
     * @param $leadingNumber
     * @return mixed|string
     */
    public static function removeLeadingNumbers($string , $leadingNumber)
    {
        $value = ltrim($string, $leadingNumber);
        return $value ?: $leadingNumber;
    }

    /**
     * Extract values from row and push them to an array
     * @param $row
     * @return array
     */
    public static function extractValuesFromRow($row): array
    {
        $sqlValues = [];

        foreach (self::COLUMNS as $column) {
            if (is_numeric($row[$column] ?? false)) {
                $sqlValues[$column] = self::removeLeadingNumbers($row[$column], 0); // Removing leading zeros from field
            }
        }

        return $sqlValues;
    }

    /**
     * Checks length of values given in a array
     * @param $countOfValues
     * @param $sqlValues
     * @param $length
     * @return bool
     */
    public static function checkLengthOfValues($countOfValues, $sqlValues, $length): bool
    {
        if ($countOfValues !== $length) {
            self::logMessage("Number of values should be {$length}, got {$countOfValues} values, Data:[" . json_encode($sqlValues) . ']');
            return false;
        }
        return true;
    }

    /**
     * @param $sqlInsert
     * @param int $chunkLength
     */
    public static function insertRowsToDataTable($sqlInsert, $chunkLength = 10000){
        $mysql = new Mysql(DB_HOST, DB_USERNAME, DB_PWD, DB_NAME);

        foreach (array_chunk($sqlInsert, $chunkLength) as $chunked) {
            $mysql->insert('INSERT INTO data (year, age, ethnic, sex, area, count) VALUES ' . implode(',', $chunked));
        }

        $mysql->closeConnection();
    }

    /**
     * @param $fileName
     */
    public static function FetchDataAndInsert($fileName)
    {
        ini_set('memory_limit', '-1');
        $sqlInsert = [];
        $file = self::getFileWithoutHeaders($fileName);

        while ($row = fgetcsv($file)) {
            $sqlValues = self::extractValuesFromRow($row);
            if (self::checkLengthOfValues(count($sqlValues), $sqlValues,6)){ // Count of fields should be in insert query
                $sqlInsert [] = '(' . implode(', ', $sqlValues) . ')';
            }
        }

        fclose($file);

        self::insertRowsToDataTable($sqlInsert);
    }

}