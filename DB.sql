CREATE TABLE ethnic
(
    code        varchar(11),
    description varchar(255),
    sort_order  int(11),

    PRIMARY KEY (code, sort_order)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

CREATE TABLE area
(
    code        varchar(11),
    description varchar(255),
    sort_order  int(11),

    PRIMARY KEY (code, sort_order)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

CREATE TABLE sex
(
    code        varchar(11),
    description varchar(255),
    sort_order  int(11),

    PRIMARY KEY (code, sort_order)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

CREATE TABLE age
(
    code        varchar(11),
    description varchar(255),
    sort_order  int(11),

    PRIMARY KEY (code, sort_order)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

CREATE TABLE year
(
    code        varchar(11),
    description varchar(255),
    sort_order  int(11),

    PRIMARY KEY (code, sort_order)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

CREATE TABLE data
(
    year   varchar(11),
    age    varchar(11),
    ethnic varchar(11),
    sex    varchar(11),
    area   varchar(11),
    count  decimal(11, 0),

    INDEX year_code (year),
    CONSTRAINT fk_year FOREIGN KEY (year)
        REFERENCES year (code),

    INDEX age_code (age),
    CONSTRAINT fk_age FOREIGN KEY (age)
        REFERENCES age (code),

    INDEX ethnic_code (ethnic),
    CONSTRAINT fk_ethnic FOREIGN KEY (ethnic)
        REFERENCES ethnic (code),

    INDEX sex_code (sex),
    CONSTRAINT fk_sex FOREIGN KEY (sex)
        REFERENCES sex (code),

    INDEX area_code (area),
    CONSTRAINT fk_area FOREIGN KEY (area)
        REFERENCES area (code)

) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;