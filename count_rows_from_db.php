<?php

require_once 'data-params.php';
require_once 'Mysql.php';

$mysql = New Mysql(DB_HOST, DB_USERNAME, DB_PWD, DB_NAME);

$query = "SELECT COUNT(*) AS rows_count  FROM data d
JOIN area ON area.code = d.area
JOIN age ON age.code = d.age
JOIN sex ON sex.code = d.sex
JOIN year ON year.code = d.year
JOIN ethnic on ethnic.code = d.ethnic

WHERE area.description = 'Hampstead'
AND age.sort_order > (SELECT sort_order FROM age WHERE description = '45 years')
AND sex.description = 'Female'
AND year.description = '2018'
AND ethnic.description = 'Asian'";

$res =  $mysql->fetch($query);
print_r($res);